package com.oas.oas.systems.mocks;

import com.oas.oas.systems.OrderSystem;
import com.oas.oas.systems.ZoneSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope(value = "singleton")
public class MockZoneSystem implements ZoneSystem {

    Logger logger = LoggerFactory.getLogger(MockZoneSystem.class);
    @Value("${oas.zone.position.max}")
    int maxPosition;

    @Value("${oas.zone.max}")
    int maxZones;


    List<Integer> zoneDistribution = new ArrayList<>();

    /**
     * Zones are added uniformly, which is not the actual case but wont affect our testcases
     */
    @PostConstruct
    public void init(){
        int allocatedPosition= this.maxPosition;
        int allocatedZones =0;
        for(int i=0;i<maxZones-1;i++) {
            zoneDistribution.add((maxPosition*(i+1))/maxZones);
        }
        zoneDistribution.add(maxPosition);
        logger.debug("distribution: {}", zoneDistribution);
    }

    @Override
    public String getZoneIdFromPosition(double position) {
        for(int i=0;i<zoneDistribution.size();i++){
            if(zoneDistribution.get(i)>position){
                return "zone"+i;
            }
        }

        throw new RuntimeException("Unservicable Zone");
    }
}
