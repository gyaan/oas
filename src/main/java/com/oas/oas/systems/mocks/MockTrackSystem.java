package com.oas.oas.systems.mocks;

import com.oas.oas.config.Constant;
import com.oas.oas.core.model.*;
import com.oas.oas.systems.TrackingSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@Scope(value = "singleton")
public class MockTrackSystem implements TrackingSystem, Runnable  {
    Logger logger = LoggerFactory.getLogger(MockTrackSystem.class);


    @Value("${oas.deliveryboy.count}")
    int deliveryBoyCount ;

    @Value("${oas.zone.position.max}")
    int maxPosition;

    @Value("${oas.deliveryboy.speed}")
    int speed;

    @Autowired
    KafkaTemplate<String, DeliveryEvent> kafkaTemplate;

    ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private volatile boolean running = true;

    Map<String, DeliveryBoy> deliveryBoyMap = new HashMap<>();
    Map<String, List<DeliveryBoy>> zoneToDeliveryBoyMap = new Hashtable<>();
    List<DeliveryBoy> movingDeliveryBoys = new Vector<>();

    @Autowired
    MockZoneSystem mockZoneSystem;

    @PostConstruct

    public void init(){
        executorService.scheduleAtFixedRate(this, 20,20, TimeUnit.MILLISECONDS);
        //Setting up random location delivery boys
        Random random = new SecureRandom();
        for(int i=0;i<deliveryBoyCount;i++){
            DeliveryBoy deliveryBoy = new DeliveryBoy();
            deliveryBoy.setId("delivery_boy"+i);
            FakeLocation fakeLocation = new FakeLocation();
            fakeLocation.setPosition(random.nextInt(maxPosition));
            String zoneId = mockZoneSystem.getZoneIdFromPosition(fakeLocation.getPosition());
            fakeLocation.setZoneId(zoneId);
            deliveryBoy.setLocation(fakeLocation);
            deliveryBoyMap.put("delivery_boy"+i, deliveryBoy);

            if(zoneToDeliveryBoyMap.get(zoneId)==null){
                zoneToDeliveryBoyMap.put(zoneId,new Vector<>());
            }
            zoneToDeliveryBoyMap.get(zoneId).add(deliveryBoy);
        }

    }

    @Override
    public List<DeliveryBoy> getDeliveryBoysForZone(String zone) {
        return zoneToDeliveryBoyMap.get(zone);
    }

    @Override
    public void startSimulation(String deliveryBoyId, DeliveryTask task) {
        DeliveryBoy deliveryBoy =deliveryBoyMap.get(deliveryBoyId);
        deliveryBoy.setDestination(task.getSource());
        deliveryBoy.setTask(task);
        task.setState(DeliveryTask.State.FOR_PICKUP);
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                movingDeliveryBoys.add(deliveryBoy);
            }
        });
    }

    @Override
    public void run() {
        Random rand = new SecureRandom();

        //Simulation of delivery boy, once order is assigned , he should first reach restaurant to pickup then to delivery location.
        //Code is duplicated can be optimised, So at delivery boy initially once order is allocated destination will be pickup point.
        // Which will change to delivery point after he reaches there.


        List<DeliveryBoy> doneDeliveryBoys = new ArrayList<>();
        for(DeliveryBoy deliveryBoy: movingDeliveryBoys){
            logger.debug("Delivery boy Location: {}",deliveryBoy);
            if(deliveryBoy.getDestination().getPosition()> deliveryBoy.getLocation().getPosition()){
                FakeLocation fakeLocation = new FakeLocation();
                //Assuming speed of all delivery boy and all terrain are same
                fakeLocation.setPosition(deliveryBoy.getLocation().getPosition()+speed);
                fakeLocation.setZoneId(mockZoneSystem.getZoneIdFromPosition(fakeLocation.getPosition()));
                //Delivey boy might have to switch zones while deliverying
                if(fakeLocation.getZoneId()!= deliveryBoy.getLocation().getZoneId()){
                    zoneToDeliveryBoyMap.get(deliveryBoy.getLocation().getZoneId()).remove(deliveryBoy);
                    zoneToDeliveryBoyMap.get(fakeLocation.getZoneId()).remove(deliveryBoy);
                }
                deliveryBoy.setLocation(fakeLocation);
                //
                if(Math.abs((fakeLocation.getPosition()-deliveryBoy.getDestination().getPosition()))<=200){
                    if(deliveryBoy.getTask().getState()== DeliveryTask.State.FOR_DELIVERY) {
                        DeliveryEvent deliveryEvent = new DeliveryEvent();
                        deliveryEvent.setDeliveryTask(deliveryBoy.getTask());
                        deliveryEvent.setCompletionTime(new Date());
                        kafkaTemplate.send(Constant.DELIVETY_TOPIC, deliveryEvent);
                        doneDeliveryBoys.add(deliveryBoy);
                        logger.info("Delivered Up: {}", deliveryBoy);
                    }else{
                        logger.info("Picked Up: {}", deliveryBoy);
                        deliveryBoy.getTask().setState(DeliveryTask.State.FOR_DELIVERY);
                        deliveryBoy.setDestination(deliveryBoy.getTask().getDestination());
                    }
                }
            }else{
                FakeLocation fakeLocation = new FakeLocation();
                //Assuming speed of all delivery boy and all terrain are same
                fakeLocation.setPosition(deliveryBoy.getLocation().getPosition()-speed);
                fakeLocation.setZoneId(mockZoneSystem.getZoneIdFromPosition(fakeLocation.getPosition()));
                if(fakeLocation.getZoneId()!= deliveryBoy.getLocation().getZoneId()){
                    zoneToDeliveryBoyMap.get(deliveryBoy.getLocation().getZoneId()).remove(deliveryBoy);
                    zoneToDeliveryBoyMap.get(fakeLocation.getZoneId()).remove(deliveryBoy);
                }
                deliveryBoy.setLocation(fakeLocation);
                    if(Math.abs((fakeLocation.getPosition()-deliveryBoy.getDestination().getPosition()))<=200){
                        if(deliveryBoy.getTask().getState()== DeliveryTask.State.FOR_DELIVERY) {
                        DeliveryEvent deliveryEvent = new DeliveryEvent();
                        deliveryEvent.setDeliveryTask(deliveryBoy.getTask());
                        deliveryEvent.setCompletionTime(new Date());
                        kafkaTemplate.send(Constant.DELIVETY_TOPIC, deliveryEvent);
                        doneDeliveryBoys.add(deliveryBoy);
                            logger.info("Delivered Up: {}", deliveryBoy);
                    }else{
                            logger.info("Picked Up: {}", deliveryBoy);
                        deliveryBoy.getTask().setState(DeliveryTask.State.FOR_DELIVERY);
                        deliveryBoy.setDestination(deliveryBoy.getTask().getDestination());
                    }
                }
            }
        }
        movingDeliveryBoys.removeAll(doneDeliveryBoys);

        logger.debug("Exiting Tracking simulator");
    }
}
