package com.oas.oas.systems.mocks;

import com.oas.oas.config.Constant;
import com.oas.oas.systems.OrderSystem;
import com.oas.oas.core.model.FakeLocation;
import com.oas.oas.core.model.Order;
import com.oas.oas.core.model.Restaurant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Random;
import java.util.UUID;


@Component
@Scope(value = "singleton")
public class MockOrderSystem extends Thread implements OrderSystem {

    Logger logger = LoggerFactory.getLogger(OrderSystem.class);

    @Autowired
    KafkaTemplate<String,Order> kafkaTemplate;

    @Autowired
    MockZoneSystem mockZoneSystem;

    @Value("${oas.zone.position.max}")
    int maxPosition;

    private volatile boolean running = true;
    @Override
    public void run() {
        Random rand = new SecureRandom();
        while(running){


            Order order = new Order();
            order.setId(UUID.randomUUID().toString());
            //Random location for order delivery
            int position= rand.nextInt(maxPosition);
            order.setLocation(new FakeLocation( position,  mockZoneSystem.getZoneIdFromPosition(position)));
            order.setCreationTime(new Date());
            //Random location for Restaurant
            int restaurantPosition= rand.nextInt(maxPosition);
            order.setRestaurant(new Restaurant(UUID.randomUUID().toString(),new FakeLocation(restaurantPosition, mockZoneSystem.getZoneIdFromPosition(restaurantPosition))));
            //Sending to kafka bus with partitioned on zoneId, In real prod app this should be ackd at each order. Not doing in simulation
            kafkaTemplate.send(Constant.ORDER_TOPIC,order.getRestaurant().getLocation().getZoneId(),order);
            logger.debug("Order sent:{} ",order);
            //Sleep for max of two seconds
            try {
                Thread.sleep(rand.nextInt(2000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @PreDestroy
    public void onDestroy() throws Exception {
        running = true;
    }

}
