package com.oas.oas.systems;

import com.oas.oas.core.model.DeliveryBoy;
import com.oas.oas.core.model.DeliveryTask;

import java.util.List;

public interface TrackingSystem {

    /**
     *  Returns Delivery Boys in a given zoneId. Irrespective of their states,
     *  This can be optimised, by filtering by state
     * @param zoneId
     * @return
     */
    public List<DeliveryBoy> getDeliveryBoysForZone(String zoneId);

    /**
     * Not actual tracking system api. here its just to start simulation
     * @param deliveryBoyId
     */
    public void  startSimulation(String deliveryBoyId, DeliveryTask task);
}
