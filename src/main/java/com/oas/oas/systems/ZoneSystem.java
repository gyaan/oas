package com.oas.oas.systems;

import com.oas.oas.core.exception.UnServiceableZoneException;

public interface ZoneSystem {
    /**
     * Returns zoneId from given position
     * @param position
     * @return
     * @throws UnServiceableZoneException
     */
    String getZoneIdFromPosition(double position) throws UnServiceableZoneException;
}
