package com.oas.oas.config;

import com.oas.oas.processor.assigners.GreedyAlgorithmOrderAssignment;
import com.oas.oas.processor.OrderAssigner;
import com.oas.oas.systems.OrderSystem;
import com.oas.oas.systems.TrackingSystem;
import com.oas.oas.systems.ZoneSystem;
import com.oas.oas.systems.mocks.MockOrderSystem;
import com.oas.oas.systems.mocks.MockTrackSystem;
import com.oas.oas.systems.mocks.MockZoneSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.kafka.annotation.EnableKafka;

@Configuration
@EnableKafka
@EnableJpaRepositories
public class OSAConfig {

    @Autowired
    MockOrderSystem mockOrderSystem;

    @Autowired
    MockZoneSystem mockZoneSystem;

    @Autowired
    MockTrackSystem mockTrackSystem;


    @Bean
    OrderSystem orderSystem(){
        mockOrderSystem.start();
        return mockOrderSystem;
    }


    @Bean
    TrackingSystem trackingSystem(){
        return mockTrackSystem;
    }

    @Bean
    ZoneSystem zoneSystem(){
        return mockZoneSystem;
    }

    @Bean
    OrderAssigner orderAssigner(){
        return new GreedyAlgorithmOrderAssignment();
    }
}
