package com.oas.oas.entity;

import com.oas.oas.core.model.DeliveryBoyStatus;
import com.oas.oas.core.model.DeliveryStatus;
import com.oas.oas.core.model.FakeLocation;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(indexes={@Index(columnList ="orderId")})
public class OrderAssignmentInfo {

    @Id
    String orderId;

    String deliveryBoyId;

    @Enumerated(value = EnumType.STRING)
    DeliveryStatus status;

    @Temporal(TemporalType.TIMESTAMP)
    Date orderTime;

    @Temporal(TemporalType.TIMESTAMP)
    Date assignmentTime;

    @Temporal(TemporalType.TIMESTAMP)
    Date deliveryTime;

    public String getDeliveryBoyId() {
        return deliveryBoyId;
    }

    public void setDeliveryBoyId(String deliveryBoyId) {
        this.deliveryBoyId = deliveryBoyId;
    }

    public DeliveryStatus getStatus() {
        return status;
    }

    public void setStatus(DeliveryStatus status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getAssignmentTime() {
        return assignmentTime;
    }

    public void setAssignmentTime(Date assignmentTime) {
        this.assignmentTime = assignmentTime;
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }
}
