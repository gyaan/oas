package com.oas.oas.entity;

import com.oas.oas.core.model.DeliveryBoyStatus;

import javax.persistence.*;

@Entity
@Table(indexes = {@Index(columnList = "deliveryBoyId")})
public class DeliveryBoyStatusInfo {
    @Id
    String deliveryBoyId;

    @Enumerated(EnumType.STRING)
    DeliveryBoyStatus status;

    public String getDeliveryBoyId() {
        return deliveryBoyId;
    }

    public void setDeliveryBoyId(String deliveryBoyId) {
        this.deliveryBoyId = deliveryBoyId;
    }

    public DeliveryBoyStatus getStatus() {
        return status;
    }

    public void setStatus(DeliveryBoyStatus status) {
        this.status = status;
    }
}
