package com.oas.oas;

import com.oas.oas.systems.OrderSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
@EnableJpaRepositories
public class OasApplicationRunner implements CommandLineRunner {

    @Autowired
    OrderSystem orderSystem;

    public static void main(String[] args) {
        SpringApplication.run(OasApplicationRunner.class, args);
    }

    @Override
    public void run(String... args) {
        System.out.println("IN run");
    }
}

