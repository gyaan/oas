package com.oas.oas.repository;

import com.oas.oas.entity.DeliveryBoyStatusInfo;
import com.oas.oas.entity.OrderAssignmentInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface OrderAssignmentRepository extends CrudRepository<OrderAssignmentInfo,Long> {

    List<OrderAssignmentInfo> findAllByOrderIdIn(List<String> orderIds);

    @Query("select COUNT(orderId),AVG(assignmentTime-orderTime),AVG(deliveryTime-assignmentTime),status from OrderAssignmentInfo GROUP BY STATUS ")
    List<Object> getStats();
}
