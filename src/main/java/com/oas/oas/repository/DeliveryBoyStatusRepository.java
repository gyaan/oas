package com.oas.oas.repository;

import com.oas.oas.core.model.DeliveryBoyStatus;
import com.oas.oas.entity.DeliveryBoyStatusInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface DeliveryBoyStatusRepository extends CrudRepository< DeliveryBoyStatusInfo,Long> {
    List<DeliveryBoyStatusInfo> findAllDeliveryBoyStatusInfoByDeliveryBoyIdInAndAndStatus(List<String> deliveryBoyIds, DeliveryBoyStatus status);
}
