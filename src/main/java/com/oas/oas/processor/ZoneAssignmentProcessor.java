package com.oas.oas.processor;

import com.oas.oas.core.model.*;
import com.oas.oas.entity.DeliveryBoyStatusInfo;
import com.oas.oas.entity.OrderAssignmentInfo;
import com.oas.oas.repository.DeliveryBoyStatusRepository;
import com.oas.oas.repository.OrderAssignmentRepository;
import com.oas.oas.systems.TrackingSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.concurrent.Callable;

@Service
@Scope(value = "prototype")
public class ZoneAssignmentProcessor implements Callable<Boolean> {

    Logger logger = LoggerFactory.getLogger(ZoneAssignmentProcessor.class);

    private String zoneId;
    @Autowired
    private OrderAssigner orderAssigner;
    private List<Order> orders;
    private List<DeliveryBoy> deliveryBoysList;

    @Autowired
    KafkaTemplate<String,Order> kafkaTemplate;


    @Autowired
    TrackingSystem trackingSystem;
    @Autowired
    DeliveryBoyStatusRepository deliveryBoyStatusRepository;
    @Autowired
    OrderAssignmentRepository orderAssignmentRepository;

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public OrderAssigner getOrderAssigner() {
        return orderAssigner;
    }

    public void setOrderAssigner(OrderAssigner orderAssigner) {
        this.orderAssigner = orderAssigner;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public List<DeliveryBoy> getDeliveryBoysList() {
        return deliveryBoysList;
    }

    public void setDeliveryBoysList(List<DeliveryBoy> deliveryBoysList) {
        this.deliveryBoysList = deliveryBoysList;
    }

    public TrackingSystem getTrackingSystem() {
        return trackingSystem;
    }

    public void setTrackingSystem(TrackingSystem trackingSystem) {
        this.trackingSystem = trackingSystem;
    }

    public DeliveryBoyStatusRepository getDeliveryBoyStatusRepository() {
        return deliveryBoyStatusRepository;
    }

    public void setDeliveryBoyStatusRepository(DeliveryBoyStatusRepository deliveryBoyStatusRepository) {
        this.deliveryBoyStatusRepository = deliveryBoyStatusRepository;
    }

    public OrderAssignmentRepository getOrderAssignmentRepository() {
        return orderAssignmentRepository;
    }

    public void setOrderAssignmentRepository(OrderAssignmentRepository orderAssignmentRepository) {
        this.orderAssignmentRepository = orderAssignmentRepository;
    }

    @Override
    @Transactional
    public Boolean call() throws Exception {

        //Get all deliveryboys from a zoneid
        deliveryBoysList = trackingSystem.getDeliveryBoysForZone(zoneId);
        List<String> deliveryBoysIds = new ArrayList<>();
        for(DeliveryBoy deliveryBoy : deliveryBoysList){
            deliveryBoysIds.add(deliveryBoy.getId());
        }

        //Filter the delivery boys if they are already engaged
        List<DeliveryBoyStatusInfo> deliveryBoyStatusInfos = deliveryBoyStatusRepository.findAllDeliveryBoyStatusInfoByDeliveryBoyIdInAndAndStatus(deliveryBoysIds, DeliveryBoyStatus.ENGAGED);

        Set<String> availableDeliveryBoysId = new HashSet<>();
        availableDeliveryBoysId.addAll(deliveryBoysIds);
        for(DeliveryBoyStatusInfo info: deliveryBoyStatusInfos){
            availableDeliveryBoysId.remove(info.getDeliveryBoyId());
        }
        List<DeliveryBoy> availableDeliveryBoys = new ArrayList<>();
        for(DeliveryBoy deliveryBoy: deliveryBoysList){
            if(availableDeliveryBoysId.contains(deliveryBoy.getId())){
                availableDeliveryBoys.add(deliveryBoy);
            }
        }
        //Call Assigner to assign the order
        orders = orderAssigner.assign(orders,availableDeliveryBoys);
        logger.debug("Assignment: {}", orders);
        List<OrderAssignmentInfo> orderAssignmentInfos = new ArrayList<>();
        List<String> deliveryBoyIds =new ArrayList<>();

        //Trigger tracking system to simulate the deliveryboys.
        //In real application this will not be done like this.
        // May be this systems emits one events which notifies the delivery boys. and they start moving
        for(Order order: orders){
            if(order.getAssignedDeliveryBoy()!=null) {
                OrderAssignmentInfo orderAssignmentInfo = new OrderAssignmentInfo();
                orderAssignmentInfo.setDeliveryBoyId(order.getAssignedDeliveryBoy().getId());
                orderAssignmentInfo.setOrderId(order.getId());
                orderAssignmentInfo.setStatus(DeliveryStatus.ASSIGNED);
                orderAssignmentInfo.setOrderTime(order.getCreationTime());
                orderAssignmentInfo.setAssignmentTime(new Date());
                orderAssignmentInfos.add(orderAssignmentInfo);
                trackingSystem.startSimulation(orderAssignmentInfo.getDeliveryBoyId(),
                        new DeliveryTask(order.getId(),
                                order.getRestaurant().getLocation(),
                                order.getLocation(),
                                DeliveryTask.State.FOR_PICKUP,order.getAssignedDeliveryBoy().getId()));
                deliveryBoyIds.add(order.getAssignedDeliveryBoy().getId());
            }else{

                //There can be orders with no delivery boys available. we might have to retry or cancel them ,asyncly
                OrderAssignmentInfo orderAssignmentInfo = new OrderAssignmentInfo();
                orderAssignmentInfo.setOrderId(order.getId());
                orderAssignmentInfo.setStatus(DeliveryStatus.UNASSIGNED);
                orderAssignmentInfo.setOrderTime(order.getCreationTime());
                orderAssignmentInfos.add(orderAssignmentInfo);
                logger.error("Order unassigned: {}", order);
                //REQUEUE or CANCEL UNASSIGNED orders
                //Ideally the numbers should be less
            }
        }

        //Save assignment status in DB
        orderAssignmentRepository.saveAll(orderAssignmentInfos);
        deliveryBoyStatusInfos= new ArrayList<>();
        for(String id: deliveryBoyIds){
            DeliveryBoyStatusInfo info = new DeliveryBoyStatusInfo();
            info.setDeliveryBoyId(id);
            info.setStatus(DeliveryBoyStatus.ENGAGED);
            deliveryBoyStatusInfos.add(info);
        }
        //Save delivery boys status in DB
        deliveryBoyStatusRepository.saveAll(deliveryBoyStatusInfos);

        return true;
    }
}
