package com.oas.oas.processor;

import com.oas.oas.core.model.*;
import com.oas.oas.entity.DeliveryBoyStatusInfo;
import com.oas.oas.entity.OrderAssignmentInfo;
import com.oas.oas.repository.DeliveryBoyStatusRepository;
import com.oas.oas.repository.OrderAssignmentRepository;
import com.oas.oas.systems.TrackingSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.concurrent.Callable;

@Service
@Scope(value = "prototype")
public class ZoneDeliveryProcessor implements Callable<Boolean> {

    Logger logger = LoggerFactory.getLogger(ZoneDeliveryProcessor.class);

    private String zoneId;

    List<DeliveryEvent> deliveryEvents;


    @Autowired
    TrackingSystem trackingSystem;
    @Autowired
    DeliveryBoyStatusRepository deliveryBoyStatusRepository;
    @Autowired
    OrderAssignmentRepository orderAssignmentRepository;

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public List<DeliveryEvent> getDeliveryEvents() {
        return deliveryEvents;
    }

    public void setDeliveryEvents(List<DeliveryEvent> deliveryEvents) {
        this.deliveryEvents = deliveryEvents;
    }

    public TrackingSystem getTrackingSystem() {
        return trackingSystem;
    }

    public void setTrackingSystem(TrackingSystem trackingSystem) {
        this.trackingSystem = trackingSystem;
    }

    public DeliveryBoyStatusRepository getDeliveryBoyStatusRepository() {
        return deliveryBoyStatusRepository;
    }

    public void setDeliveryBoyStatusRepository(DeliveryBoyStatusRepository deliveryBoyStatusRepository) {
        this.deliveryBoyStatusRepository = deliveryBoyStatusRepository;
    }

    public OrderAssignmentRepository getOrderAssignmentRepository() {
        return orderAssignmentRepository;
    }

    public void setOrderAssignmentRepository(OrderAssignmentRepository orderAssignmentRepository) {
        this.orderAssignmentRepository = orderAssignmentRepository;
    }

    @Override
    @Transactional
    public Boolean call() throws Exception {
        List<String> orderIds = new ArrayList<>();
        List<String> deliverBoyIds = new ArrayList<>();
        List<DeliveryBoyStatusInfo> deliveryBoyStatusInfos = new ArrayList<>();
        for(DeliveryEvent de: deliveryEvents){
            orderIds.add(de.getDeliveryTask().getOrderId());
            deliverBoyIds.add(de.getDeliveryTask().getDeliveryBoyId());
            DeliveryBoyStatusInfo dv= new DeliveryBoyStatusInfo();
            dv.setStatus(DeliveryBoyStatus.AVAILABLE);
            dv.setDeliveryBoyId(de.getDeliveryTask().getDeliveryBoyId());
            deliveryBoyStatusInfos.add(dv);
        }

        List<OrderAssignmentInfo> orderAssignmentInfos =orderAssignmentRepository.findAllByOrderIdIn(orderIds);
        for(OrderAssignmentInfo assignmentInfo: orderAssignmentInfos){
            assignmentInfo.setStatus(DeliveryStatus.DELIVERED);
            assignmentInfo.setDeliveryTime(new Date());
        }

        orderAssignmentRepository.saveAll(orderAssignmentInfos);
        deliveryBoyStatusRepository.saveAll(deliveryBoyStatusInfos);


        return true;
    }
}
