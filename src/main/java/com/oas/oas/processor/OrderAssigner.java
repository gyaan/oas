package com.oas.oas.processor;

import com.oas.oas.core.model.DeliveryBoy;
import com.oas.oas.core.model.Order;

import java.util.List;

/**
 * OrderAssigner is abstraction of computation entity which gets all data required to compute and assign in APIS.
 * Implementation should be a stateless  , so not state should be maintained strictly to avoid any concurrency issues
 *
 *This allows to experiment with multiple algorithms and allocation strategies
 */
public interface OrderAssigner {

    /**
     * Assign deliveryBoy to orders
     * @param orders
     * @param deliveryBoys
     * @return
     */
    List<Order> assign(List<Order> orders, List<DeliveryBoy> deliveryBoys);
}
