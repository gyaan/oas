package com.oas.oas.processor;

import com.oas.oas.core.model.DeliveryEvent;
import com.oas.oas.core.model.Order;
import com.oas.oas.repository.OrderAssignmentRepository;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Component
@Scope("singleton")
public class OrderAssignmentDispatcher extends Thread{

    ExecutorService executors = Executors.newCachedThreadPool();
    Logger logger = LoggerFactory.getLogger(OrderAssignmentDispatcher.class);

    @Autowired
    BeanFactory beanFactory;

    @Autowired
    OrderAssignmentRepository orderAssignmentRepository;

    /**
     * Order assignment topic listener
     * @param records
     * @param acknowledgment
     */
    @KafkaListener(topics = "ORDER_FLOW",concurrency = "10")
    public void orderWithHeadersListener(
            List<ConsumerRecord<String, Order>> records, Acknowledgment acknowledgment) {
        logger.debug("Record {}",records);
        logger.debug("Record Length {} , Partiton {}",records.size(), records.get(0).partition());


        //One consumer can get multiple partition data. so segregate the batch on zoneid
        Map<String,List<Order>> zoneToOrder = new HashMap<>();
        for(ConsumerRecord<String,Order> cr: records){
            if(zoneToOrder.get(cr.key())==null){
                zoneToOrder.put(cr.value().getRestaurant().getLocation().getZoneId(),new ArrayList<>());
            }
            zoneToOrder.get(cr.value().getRestaurant().getLocation().getZoneId()).add(cr.value());
        }

        //For each zone process the the order
        List<Future<Boolean>> futures = new ArrayList<>();
        for(Map.Entry<String,List<Order>> entry: zoneToOrder.entrySet()){
            ZoneAssignmentProcessor zoneProcessor = beanFactory.getBean(ZoneAssignmentProcessor.class);
            zoneProcessor.setZoneId(entry.getKey());
            zoneProcessor.setOrders(entry.getValue());
            //Using cached executor pool
            futures.add(executors.submit(zoneProcessor));
        }


        //Wait for everything to get done
        for(Future<Boolean> future: futures){
            try{
                future.get();
            }catch (InterruptedException ex){

            }catch (ExecutionException ex){
                //TODO retry batch or move to error queue which retires further
            }
        }

        //Ack
        acknowledgment.acknowledge();
    }


}
