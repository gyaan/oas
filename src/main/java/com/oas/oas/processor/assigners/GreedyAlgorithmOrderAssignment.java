package com.oas.oas.processor.assigners;

import com.oas.oas.core.model.DeliveryBoy;
import com.oas.oas.core.model.Order;
import com.oas.oas.processor.OrderAssigner;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class GreedyAlgorithmOrderAssignment implements OrderAssigner {

    private static class Node implements Comparable<Node> {
        int value;
        int orderIndex;
        int deliveryBoyIndex;

        public Node(int value, int orderIndex, int deliveryBoyIndex) {
            this.value = value;
            this.orderIndex = orderIndex;
            this.deliveryBoyIndex = deliveryBoyIndex;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return value == node.value;
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }

        @Override
        public int compareTo(Node o) {
            return o.value-value;
        }
    }
    @Override
    public List<Order> assign(List<Order> orders, List<DeliveryBoy> deliveryBoys) {

        PriorityQueue<Node> heap= new PriorityQueue<>();
        for(int i=0;i<orders.size();i++){
            for(int j=0;j<deliveryBoys.size();j++){
                heap.add(new Node(calculateCost(orders.get(i),deliveryBoys.get(j)),i,j));
            }
        }
        Set<Integer> orderSet = new HashSet<>();
        Set<Integer> deliveryBoySet = new HashSet<>();
        while(!heap.isEmpty()){
            Node node = heap.poll();
            if(!orderSet.contains(node.orderIndex)&& !deliveryBoySet.contains(node.deliveryBoyIndex)){
                orders.get(node.orderIndex).setAssignedDeliveryBoy(deliveryBoys.get(node.deliveryBoyIndex));
                orderSet.add(node.orderIndex);
                deliveryBoySet.add(node.deliveryBoyIndex);
            }
        }

        return orders;
    }

    /**
     * This calculate cost by just distance difference but we can add more parameters to it
     * @param order
     * @param deliveryBoy
     * @return
     */
    private int calculateCost(Order order, DeliveryBoy deliveryBoy){
        return (int)Math.abs(deliveryBoy.getLocation().getPosition()-order.getRestaurant().getLocation().getPosition());
    }
}
