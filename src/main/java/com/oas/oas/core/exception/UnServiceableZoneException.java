package com.oas.oas.core.exception;

public class UnServiceableZoneException extends RuntimeException{
    String zoneId;
    public UnServiceableZoneException(String zoneId) {
        super(zoneId+" is Serviceable");
    }

    public String getZoneId() {
        return zoneId;
    }
}
