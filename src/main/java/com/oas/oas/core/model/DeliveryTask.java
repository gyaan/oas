package com.oas.oas.core.model;

public class DeliveryTask {
    public enum State{
        FOR_PICKUP,
        FOR_DELIVERY
    }
    String orderId;
    FakeLocation source;
    FakeLocation destination;
    State state;
    String deliveryBoyId;

    public DeliveryTask() {
    }

    public DeliveryTask(String orderId, FakeLocation source, FakeLocation destination, State state, String deliveryBoyId) {
        this.orderId = orderId;
        this.source = source;
        this.destination = destination;
        this.state = state;
        this.deliveryBoyId = deliveryBoyId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public FakeLocation getSource() {
        return source;
    }

    public void setSource(FakeLocation source) {
        this.source = source;
    }

    public FakeLocation getDestination() {
        return destination;
    }

    public void setDestination(FakeLocation destination) {
        this.destination = destination;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getDeliveryBoyId() {
        return deliveryBoyId;
    }

    public void setDeliveryBoyId(String deliveryBoyId) {
        this.deliveryBoyId = deliveryBoyId;
    }

    @Override
    public String toString() {
        return "DeliveryTask{" +
                "orderId='" + orderId + '\'' +
                ", source=" + source +
                ", destination=" + destination +
                ", state=" + state +
                '}';
    }
}
