package com.oas.oas.core.model;

public enum DeliveryStatus {
    UNASSIGNED, ASSIGNED,OUT_FOR_DELIVERY,DELIVERED;
}
