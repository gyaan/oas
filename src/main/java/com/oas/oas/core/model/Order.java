package com.oas.oas.core.model;


import java.util.Date;

public class Order {
    private String id;
    private FakeLocation location;
    private Restaurant restaurant;
    private DeliveryBoy assignedDeliveryBoy;
    private Date creationTime;
    private Date assignedTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FakeLocation getLocation() {
        return location;
    }

    public void setLocation(FakeLocation location) {
        this.location = location;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public DeliveryBoy getAssignedDeliveryBoy() {
        return assignedDeliveryBoy;
    }

    public void setAssignedDeliveryBoy(DeliveryBoy assignedDeliveryBoy) {
        this.assignedDeliveryBoy = assignedDeliveryBoy;
    }
    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getAssignedTime() {
        return assignedTime;
    }

    public void setAssignedTime(Date assignedTime) {
        this.assignedTime = assignedTime;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", location=" + location +
                ", restaurant=" + restaurant +
                ", assignedDeliveryBoy=" + assignedDeliveryBoy +
                ", creationTime=" + creationTime +
                ", assignedTime=" + assignedTime +
                '}';
    }
}
