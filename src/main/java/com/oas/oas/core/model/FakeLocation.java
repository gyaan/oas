package com.oas.oas.core.model;

import java.util.Objects;

/**
 * Fake location is not based on lat long instead used single numeric value to define position on
 * single dimensional space.
 *
 * As we are dealing with distance and speed this will make things eaiser for this task
 *
 * Distance between two points is abs(a-b)
 *
 */
public class FakeLocation {
    private double position;
    private String zoneId;

    public FakeLocation(double position, String zoneId) {
        this.position = position;
        this.zoneId = zoneId;
    }

    public FakeLocation() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FakeLocation that = (FakeLocation) o;
        return Double.compare(that.position, position) == 0 &&
                Objects.equals(zoneId, that.zoneId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, zoneId);
    }

    public double getPosition() {
        return position;
    }

    public void setPosition(double position) {
        this.position = position;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    @Override
    public String toString() {
        return "FakeLocation{" +
                "position=" + position +
                ", zoneId='" + zoneId + '\'' +
                '}';
    }
}
