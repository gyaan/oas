package com.oas.oas.core.model;

public class Restaurant {
    private String id;
    private FakeLocation location;

    public Restaurant(String id, FakeLocation location) {
        this.id = id;
        this.location = location;
    }

    public Restaurant() {
    }

    public String getId() {
        return id;
    }

    public FakeLocation getLocation() {
        return location;
    }

    public void setId(String id) {
        this.id = id;
    }


    public void setLocation(FakeLocation location) {
        this.location = location;

    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id='" + id + '\'' +
                ", location=" + location +
                '}';
    }
}
