package com.oas.oas.core.model;

import java.util.Objects;

public class DeliveryBoy {
    private String id;
    private FakeLocation location;

    private FakeLocation destination;

    private DeliveryTask task;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FakeLocation getLocation() {
        return location;
    }

    public void setLocation(FakeLocation location) {
        this.location = location;
    }

    public DeliveryTask getTask() {
        return task;
    }

    public FakeLocation getDestination() {
        return destination;
    }

    public void setDestination(FakeLocation destination) {
        this.destination = destination;
    }

    public void setTask(DeliveryTask task) {
        this.task = task;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeliveryBoy that = (DeliveryBoy) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "DeliveryBoy{" +
                "id='" + id + '\'' +
                ", location=" + location +
                ", destination=" + destination +
                ", task=" + task +
                '}';
    }
}
