package com.oas.oas.core.util;

import com.oas.oas.processor.ZoneAssignmentProcessor;
import com.oas.oas.repository.OrderAssignmentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
/**
 * Statistic printer
 *
 * This  prints the stats on intermittent level.
 * keeping here for quick
 */

public class StatsPrinter implements Runnable {
    Logger logger = LoggerFactory.getLogger(ZoneAssignmentProcessor.class);

    @Autowired
    OrderAssignmentRepository orderAssignmentRepository;
    ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    @PostConstruct
    public void init(){
        executorService.scheduleAtFixedRate(this,1000,1000, TimeUnit.MILLISECONDS);
    }

    public void run(){

            List<Object> objects = orderAssignmentRepository.getStats();
            if(objects!=null){
                for(Object o:objects) {
                    Object[] oo =(Object[]) o;
                    logger.info("Stats Orders: Count:{},Avg Assignment Time(s):{},Avg Delivery Time(s):{}, State:{}", oo[0],oo[1],oo[2],oo[3]);
                }
            }

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }
}
